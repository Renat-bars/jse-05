package ru.tsc.almukhametov.tm;

import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class Salutation {

    public static void main(String[] args) {
	    System.out.println("** Welcome to THE REAL WORLD **");
	    parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length ==0) return;
        final String arg = args[0];
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Renat Almukhametov");
        System.out.println("E-MAIL: rralmukhametov@tsconsulting.com");
    }

    public static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp(){
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Display developer info");
        System.out.println(TerminalConst.VERSION + " - Display program version");
        System.out.println(TerminalConst.HELP + " - Display list of commands");
    }
}
